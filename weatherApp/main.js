angular.module('app', []);
angular.module('app').config(['$controllerProvider', function($controllerProvider) {
  $controllerProvider.allowGlobals();
}]);

function WeatherController($scope, $http) {
  $scope.temperature = "";
  $scope.backgroundImage = {};
  $scope.isCelciusShown = false;
  $scope.isLocationAvailable = false;
  $scope.latitude = 0;
  $scope.longitude = 0;
  $scope.locationError = "";
  $scope.weatherData = {};
  $scope.getLocation = function() {
    if ($scope.isLocationAvailable) {
      return;
    }
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition($scope.gotPosition, $scope.getLocationError);
    }
  };
  $scope.getDataForLocation = function() {
    let myAppID = "&APPID=99b35c13cde57a75ea6d77d05b1ce138&units=metric";
    let data = "lat=" + $scope.latitude + "&lon=" + $scope.longitude + myAppID + "&callback=JSON_CALLBACK";
    let url = "http://api.openweathermap.org/data/2.5/weather?" + data;
    return $http.jsonp(url);
  };
  $scope.gotPosition = function(position) {
    $scope.isLocationAvailable = true;
    $scope.latitude = position.coords.latitude;
    $scope.longitude = position.coords.longitude;
    $scope.getDataForLocation().success(function(jsonData) {
      parseShowWeather(jsonData);
    });
  };

  function parseShowWeather(jsonData) {
    $scope.weatherData.main = jsonData.weather[0].main;
    $scope.weatherData.description = jsonData.weather[0].description;
    $scope.weatherData.metricTemp = jsonData.main.temp;
    /* Pressure in hPA on Sea level, humidiy %*/
    $scope.weatherData.pressure = jsonData.main.pressure;
    $scope.weatherData.humidity = jsonData.main.humidity;
    $scope.weatherData.wind = jsonData.wind;
    $scope.weatherData.city = jsonData.name;
    $scope.weatherData.country = jsonData.sys.country;
    $scope.weatherData.imperialTemp = ($scope.weatherData.metricTemp * 1.8) + 32;
    $scope.weatherData.imageSource = "http://openweathermap.org/img/w/" + jsonData.weather[0].icon + ".png";
    $scope.temperature = $scope.weatherData.metricTemp + " Celcius";
    var url = "http://www.visitaugusta.com/var/ezwebin_site/storage/images/media/multimedia/background-images/average-weather-background-image/84772-1-eng-GB/Average-Weather-background-image_background_image.jpg";
    switch ($scope.weatherData.main) {
      case 'Thunderstorm':
        url = "http://vignette1.wikia.nocookie.net/cardfight/images/9/98/Lightning-thunderstorm-vista-background.jpg/revision/latest?cb=20131112124822";
        break;
      case 'Drizzle':
        url = "https://darcydisconsolate.files.wordpress.com/2015/06/file000693141413.jpg";
        break;
      case 'Rain':
        url = "https://i.ytimg.com/vi/ZISJpRCtFbk/maxresdefault.jpg";
        break;
      case 'Snow':
        url = "https://i.ytimg.com/vi/l_8uHPlEtos/maxresdefault.jpg";
        break;
      case 'Atmosphere':
      case 'Fog':
        url = "http://feelgrafix.com/data_images/out/18/924726-atmosphere.jpg"
        break;
      case 'Clear':
        url = "http://www.dream-wallpaper.com/free-wallpaper/nature-wallpaper/clear-water-and-blue-sky-1-wallpaper/1920x1200/free-wallpaper-8.jpg";
        break;
      case 'Clouds':
        url = "https://aacorn.files.wordpress.com/2014/12/6924603-clouds-background-hd.jpg";
        break;
      case 'Extreme':
        url = "http://www.duskyswondersite.com/wp-content/uploads/2011/02/weather-cloud-and-lightening-by-Smith.jpg";
        break;
    }
    var url1 = 'url(' + url + ')';
    $('body').css('backgroundImage', url1);
    $scope.backgroundImage = {
      background: url1
    };
    /*
    {"coord":{"lon":17.94,"lat":59.4},"weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04d"}],"base":"cmc stations","main":{"temp":8.48,"pressure":1030,"humidity":70,"temp_min":8,"temp_max":9},"wind":{"speed":3.6,"deg":10},"clouds":{"all":75},"dt":1457967000,"sys":{"type":1,"id":5420,"message":0.0029,"country":"SE","sunrise":1457931895,"sunset":1457974238},"id":2700791,"name":"Kista","cod":200}
    */
  };
  $scope.toggleUnits = function() {
    if ($scope.isCelciusShown) {
      $scope.temperature = $scope.weatherData.metricTemp + " Celcius";
    } else {
      $scope.temperature = $scope.weatherData.imperialTemp + " Farenheit";
    }

    $scope.isCelciusShown = !$scope.isCelciusShown;
  };
  $scope.getLocationError = function(error) {
    $scope.isLocationAvailable = false;
    switch (error.code) {
      case error.PERMISSION_DENIED:
        $scope.locationError = "User denied the request for Geolocation."
        break;
      case error.POSITION_UNAVAILABLE:
        $scope.locationError = "Location information is unavailable."
        break;
      case error.TIMEOUT:
        $scope.locationError = "The request to get user location timed out."
        break;
      case error.UNKNOWN_ERROR:
        $scope.locationError = "An unknown error occurred."
        break;
    }
  };
}