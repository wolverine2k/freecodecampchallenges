angular.module('app', []);
angular.module('app').config(['$controllerProvider', function($controllerProvider) {
  $controllerProvider.allowGlobals();
}]);

function WikiController($scope, $http, $window) {
  $scope.queryObjects = {};
  $scope.getRandomWikiPage = function() {
    $window.open('https://en.wikipedia.org/wiki/Special:Random', '_blank');
  };
  $scope.openPage = function(pageId) {
    var url = "https://en.wikipedia.org/?curid=" + pageId;
    $window.open(url, '_blank');
  };
  function generateAPI(searchText) {
    var api = "https://en.wikipedia.org/w/api.php?format=json&action=query&generator=search&gsrnamespace=0&gsrlimit=10&prop=pageimages|extracts&pilimit=max&exintro&explaintext&exsentences=1&exlimit=max&gsrsearch=";
    var url = api + searchText + '&callback=JSON_CALLBACK';
    return $http.jsonp(url);
  };
  $scope.clearSearchResults = function() {
    $scope.queryObjects = {};
  };
  $scope.search = function(searchText) {
    generateAPI(searchText).success(function(jsonData) {
      console.log(jsonData.query.pages);
      $scope.queryObjects = jsonData.query.pages;
    });
  };
}
