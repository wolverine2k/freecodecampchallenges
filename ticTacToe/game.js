var canvasWidth = 300;
var canvasHeight = 300;

$('#gameCanvas').attr('width', canvasWidth);
$('#gameCanvas').attr('height', canvasHeight);

var canvas = $('#gameCanvas')[0].getContext('2d');
drawMainRectangle();

function drawMainRectangle() {
  canvas.strokeRect(0, 0, canvasWidth, canvasHeight);
  canvas.beginPath();
  canvas.moveTo(0, canvasHeight/3);
  canvas.lineTo(canvasHeight, canvasWidth/3);
  canvas.moveTo(0, (canvasHeight/3)*2);
  canvas.lineTo(canvasHeight, (canvasWidth/3)*2);
  canvas.moveTo((canvasHeight/3), 0);
  canvas.lineTo(canvasHeight/3, canvasWidth);
  canvas.moveTo((canvasHeight/3)*2, 0);
  canvas.lineTo((canvasWidth/3)*2, canvasHeight);
  
  canvas.closePath();
  canvas.stroke();
/*  
  canvas.strokeRect(0, 0, canvasWidth/3, canvasHeight/3);
  canvas.strokeRect(canvasWidth/3, 0, canvasWidth/3, canvasHeight/3);
  canvas.strokeRect(canvasWidth/3, 0, canvasWidth/3, canvasHeight/3);
  canvas.strokeRect(canvasWidth/3, canvasHeight/3, canvasWidth/3, canvasHeight/3);
  canvas.strokeRect(0, canvasHeight/3, canvasWidth/3, canvasHeight/3);
*/  
}

var FPS = 30;
setInterval(function() {
  update();
  draw();
}, 1000/FPS);

// X-Image https://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/Red_X_Freehand.svg/1024px-Red_X_Freehand.svg.png
// O-Image https://lh3.googleusercontent.com/n9c6jHhz_i5zomBtM4aRMepTVzO6I70jxFrP7Xu2VskAMO-VZTPslk5nvMjeBegiKXY

var xImage = new Image();
xImage.src = "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/Red_X_Freehand.svg/1024px-Red_X_Freehand.svg.png";
var oImage = new Image();
oImage.src = "http://www.double-o-light.com/images/leds-double-o.png";


function update() {
  
}

function draw() {
  
/*  
  canvas.drawImage(oImage, (canvasWidth/2) - (oImage.width/2), (canvasHeight/2) - (oImage.height/2));    
  canvas.drawImage(xImage, (canvasWidth/2) - (xImage.width/2), (canvasHeight/2) - (xImage.height));    
*/  
}