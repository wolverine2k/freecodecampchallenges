var OPERATIONS_STACK = [];
var TEXT_TO_SHOW = new String("0");
/* Simply checks the validity of the operation */
var VALID_OPERATIONS = {
  '+': '+',
  '-': '-',
  '*': '*',
  '/': '/'
};

var CURRENT_VALUE = 0;
var FLAG_RESET_TEXT_2_SHOW = false;
var MEMORY_CURRENT_VALUE = 0;
var MEMORY_OPERATIONS_STACK = [];
/* 0 = OP Not started, 1 = OP pressed, 2 = OP OK
	oscillate between 1 and 2. On clear/cancel/enter OP = 0
*/
var OPERATION_STATE = 0;

$(document).ready(function() {
  var keyboardEventTokeyLookup = {
    8: 'btn-backspace',
    27: 'btn-cancel',
    13: 'btn-equals',
    61: 'btn-equals',
    46: 'btn-backspace',
    8: 'btn-backspace',
    96: '0',
    97: '1',
    49: '1',
    98: '2',
    50: '2',
    99: '3',
    51: '3',
    100: '4',
    52: '4',
    101: '5',
    53: '5',
    102: '6',
    54: '6',
    103: '7',
    55: '7',
    104: '8',
    56: '8',
    105: '9',
    57: '9',
    111: 'btn-divide',
    47: 'btn-divide',
    106: 'btn-multiply',
    42: 'btn-multiply',
    109: 'btn-subtract',
    45: 'btn-subtract',
    107: 'btn-plus',
    43: 'btn-plus',
    110: 'btn-decimal',
    46: 'btn-decimal'
  };

  function printOperationsStack() {
    var opStack = new String("");
    if (OPERATIONS_STACK.length !== 0) {
      for (var i = 0; i < OPERATIONS_STACK.length; ++i) {
        opStack = opStack.concat(OPERATIONS_STACK[i]);
        opStack = opStack.concat(" ");
      }

    }
    $('#operations-stack').val(opStack.toString());
    $('#operations-stack').text(opStack.toString());
  }

  function updateTextToShow() {
    $('#results-output').val(TEXT_TO_SHOW.toString());
    $('#results-output').text(TEXT_TO_SHOW.toString());
  }

  function evaluateStack() {
    var stackResults = 0;

    if (OPERATIONS_STACK.length === 0) {
      return 0;
    }
    /*
        var opConsumed = OPERATIONS_STACK[OPERATIONS_STACK.length - 1].toString();
        if (VALID_OPERATIONS[opConsumed]) {
          newOperationState(1);
          return parseInt(TEXT_TO_SHOW.toString(), 10);
        }
    */
    for (var i = 0; i < OPERATIONS_STACK.length; ++i) {
      switch (OPERATIONS_STACK[i]) {
        case '+':
          ++i;
          if (i < OPERATIONS_STACK.length) {
            stackResults = stackResults + parseInt(OPERATIONS_STACK[i].toString(), 10);
          }
          break;
        case '-':
          ++i;
          if (i < OPERATIONS_STACK.length) {
            stackResults = stackResults - parseInt(OPERATIONS_STACK[i].toString(), 10);
          }
          break;
        case '*':
          ++i;
          if (i < OPERATIONS_STACK.length) {
            stackResults = stackResults * parseInt(OPERATIONS_STACK[i].toString(), 10);
          }
          break;
        case '/':
          ++i;
          if (i < OPERATIONS_STACK.length) {
            stackResults = stackResults / parseInt(OPERATIONS_STACK[i].toString(), 10);
          }
          break;
        default:
          stackResults = OPERATIONS_STACK[i];
          break;
      }
    }
    return stackResults;
  }

  document.addEventListener('keydown', function(event) {
    var valueOfId = keyboardEventTokeyLookup[event.keyCode];
    if (valueOfId) {
      stepCalculate(valueOfId, valueOfId);
    }
  });

  $("h1").addClass("animated bounce");
  $('#results-output').val(TEXT_TO_SHOW.toString());

  function checkHelper(value) {
    var calcValue = true;
    if (OPERATIONS_STACK.length === 0) {
      CURRENT_VALUE = parseInt(TEXT_TO_SHOW.toString(), 10);
      calcValue = false;
      OPERATIONS_STACK.push(parseInt(TEXT_TO_SHOW.toString(), 10));
      OPERATIONS_STACK.push(value);
      FLAG_RESET_TEXT_2_SHOW = true;
      return -1;
    }

    var opConsumed = OPERATIONS_STACK[OPERATIONS_STACK.length - 1].toString();
    if ((OPERATION_STATE === 1 && VALID_OPERATIONS[opConsumed]) || FLAG_RESET_TEXT_2_SHOW) {
      OPERATIONS_STACK.pop();
      OPERATIONS_STACK.push(value);
      FLAG_RESET_TEXT_2_SHOW = true;
      TEXT_TO_SHOW = new String(CURRENT_VALUE);
      return -1;
    } else if (OPERATION_STATE === 2) {
      newOperationState(0);
    }

    OPERATIONS_STACK.push(parseInt(TEXT_TO_SHOW.toString(), 10));
    OPERATIONS_STACK.push(value);
    if (calcValue) {
      CURRENT_VALUE = evaluateStack();
      /*
            switch (value) {
              case '+':
                CURRENT_VALUE = CURRENT_VALUE + parseInt(TEXT_TO_SHOW.toString(), 10);
                break;
              case '-':
                CURRENT_VALUE = CURRENT_VALUE - parseInt(TEXT_TO_SHOW.toString(), 10);
                break;
              case '*':
                CURRENT_VALUE = CURRENT_VALUE * parseInt(TEXT_TO_SHOW.toString(), 10);
                break;
              case '/':
                CURRENT_VALUE = CURRENT_VALUE / parseInt(TEXT_TO_SHOW.toString(), 10);
                break;
            }
      */
    }
    TEXT_TO_SHOW = new String(CURRENT_VALUE);

    FLAG_RESET_TEXT_2_SHOW = true;
    return 0;
  }

  function newOperationState(value) {
    if (value < 0 || value > 3) {
      /* Error Handling... */
      OPERATION_STATE = 0;
    } else {
      OPERATION_STATE = value;
    }
  }

  function stepCalculate(idOfButton, value) {
    switch (idOfButton) {
      case 'btn-mc':
        break;
      case 'btn-mr':
        break;
      case 'btn-ms':
        break;
      case 'btn-mplus':
        break;
      case 'btn-subtract':
        {
          checkHelper(VALID_OPERATIONS['-']) === 0 ? newOperationState(2) : newOperationState(1);
        }
        break;
      case 'btn-backspace':
        {
          if (TEXT_TO_SHOW.length === 1) {
            TEXT_TO_SHOW = new String(0);
          } else {
            TEXT_TO_SHOW = TEXT_TO_SHOW.substring(0, TEXT_TO_SHOW.length - 1);
          }
        }
        break;
        /* Only cancel current operation */
      case 'btn-ce':
        TEXT_TO_SHOW = new String("0");
        break;
        /* Cancel all operations. Empty all stacks if needed */
      case 'btn-cancel':
        CURRENT_VALUE = 0;
        TEXT_TO_SHOW = new String("0");
        OPERATIONS_STACK = [];
        FLAG_RESET_TEXT_2_SHOW = true;
        newOperationState(0);
        break;
      case 'btn-plusminus':
        {
          var value2Use = parseInt(TEXT_TO_SHOW.toString(), 10);
          if (value2Use > 0) {
            value2Use = 0 - value2Use;
            TEXT_TO_SHOW = new String(value2Use);
          } else {
            TEXT_TO_SHOW = TEXT_TO_SHOW.substring(1, TEXT_TO_SHOW.length);
          }
        }

        break;
      case 'btn-squareroot':
        {
          /* The stack is reset and we don't keep it anymore once SQRT is pressed */
          var value2Use = parseInt(TEXT_TO_SHOW.toString(), 10);
          var strInStack = 'sqrt(' + value2Use.toString() + ')';
          OPERATIONS_STACK.push(strInStack);
          CURRENT_VALUE = Math.sqrt(value2Use);
          TEXT_TO_SHOW = new String(CURRENT_VALUE);
          OPERATIONS_STACK = [];
          FLAG_RESET_TEXT_2_SHOW = true;
          newOperationState(0);
        }
        break;
      case 'btn-divide':
        {
          checkHelper(VALID_OPERATIONS['/']) === 0 ? newOperationState(2) : newOperationState(1);
        }
        break;
      case 'btn-modulo':
        break;
      case 'btn-invert':
				{
						var value2Use = parseFloat(TEXT_TO_SHOW.toString());
						if(value2Use !== 0) {
							CURRENT_VALUE = 1/value2Use;
              TEXT_TO_SHOW = new String(CURRENT_VALUE);
						} else {
              CURRENT_VALUE = 0;
              TEXT_TO_SHOW = new String("Infinity");
            }
            OPERATIONS_STACK = [];
            FLAG_RESET_TEXT_2_SHOW = true;
            newOperationState(0);
				}
        break;
      case 'btn-equals':
        OPERATIONS_STACK.push(parseInt(TEXT_TO_SHOW.toString(), 10));
        result = evaluateStack();
        if (result) {
          TEXT_TO_SHOW = new String(result);
        } else {
          TEXT_TO_SHOW = new String(0);
        }
        OPERATIONS_STACK = [];
        FLAG_RESET_TEXT_2_SHOW = true;
        newOperationState(0);
        break;
      case 'btn-multiply':
        {
          checkHelper(VALID_OPERATIONS['*']) === 0 ? newOperationState(2) : newOperationState(1);
        }
        break;
      case 'btn-plus':
        {
          checkHelper(VALID_OPERATIONS['+']) === 0 ? newOperationState(2) : newOperationState(1);
        }
        break;
      case 'btn-decimal':
        break;
      default:
        {
          if (FLAG_RESET_TEXT_2_SHOW || TEXT_TO_SHOW.localeCompare("0") === 0) {
            FLAG_RESET_TEXT_2_SHOW = false;
            TEXT_TO_SHOW = value;
            if (OPERATION_STATE === 1) {
              newOperationState(2);
            }
          } else if (TEXT_TO_SHOW.localeCompare("0") !== 0) {
            TEXT_TO_SHOW = TEXT_TO_SHOW.concat(value);
          }
        }
        break;
    }
    printOperationsStack();
    updateTextToShow();
  }

  /* Capture all the various button clicks.... */
  $("button").click(function() {
    stepCalculate(this.id, $(this).text());
  });

});
