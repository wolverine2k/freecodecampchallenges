/*$(document).ready(function() {
  $("h1").addClass("animated bounce");
});*/

/*http://forismatic.com/en/api/
http://quotes.stormconsultancy.co.uk/random.json
http://api.icndb.com/jokes/random
https://market.mashape.com/andruxnet/random-famous-quotes
*/
$(document).ready(function() {

  var quote="";
  var author="";
  
  $("#tweetQuote").on('click', function(event) {
    var url="https://twitter.com/intent/tweet?text=" + encodeURIComponent(quote+author) + "&hashtags=quotes,freecodecamp&via=wolverine2k";
    var newWin = window.open(url, '_blank');
    newWin.focus();
  });
  
  function getQuotesFromServer() {
    var url = 'http://api.forismatic.com/api/1.0/?method=getQuote&format=jsonp&lang=en&jsonp=?';

    $.ajax({
      type: 'GET',
      url: url,
      async: false,
      jsonpCallback: 'jsonCallback',
      contentType: "application/json",
      dataType: 'jsonp',
      success: function(json) {
        quote = json["quoteText"];
        author = json["quoteAuthor"];
        if (author == "") {
          author = "Unknown";
        }
        $("#quotePlace").text(quote);
        $("#authorPlace").text("- " + author);
      },
      error: function(e) {
        $("#quotePlace").text(e);
      }
    })(jQuery);
  }

  $("#getAnotherQuote").on('click', function(event) {
    getQuotesFromServer();
  });

  window.onload = getQuotesFromServer();
});