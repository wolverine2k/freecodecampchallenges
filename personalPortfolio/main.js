$(document).ready(function() {
  $("h1").addClass("animated bounce");
});

/*
$('#myModal').click(function() {
  $('[data-toggle="popover"]').popover();
  $('#home').navigate();
});
*/

$(document).ready(function() {
  $("#contactMeUsingSocialButtons").hide();
  $("#home").click(function navigate() {
    $("#home").navigate();
  });

  $("#contact").click(function showAlert() {
    $('html,body').scrollTop(0);
    $("#contactMeUsingSocialButtons").show();
    $("#contactMeUsingSocialButtons").fadeTo(2000, 500).slideUp(500, function() {
      $("#contactMeUsingSocialButtons").hide();
    });
  });
});