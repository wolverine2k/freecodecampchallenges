### Pomodoro Clock Angular2 Plunker - Typescript - RC.0

Derived from Angular2 Starter Plunker & Typescript

A simple plunker demonstrating Angular2 usage:
- Uses SystemJS + TypeScript to compile on the fly
- Includes binding, directives, http, pipes, and DI usage.
