//our root app component
import {Component, Input, Output, EventEmitter} from '@angular/core'

@Component({
  selector: 'session-length',
  providers: [],
  template: `
    <div class="ui circular segment center">
      <h2>Session Time:</h2>
      <h3><input name="sessionTime" #sessionTimeValue type="number" min="1" max="60" [disabled]="sessionDisabled" (change)="onValueChanged(sessionTimeValue)" value={{sessionTime}}> minutes</h3>
    </div>
  `,
  directives: []
})

export class Session {
  @Input() sessionTime = 0;
  @Input() sessionDisabled = false;
  @Output() changedSessionTime = new EventEmitter();
  onValueChanged(sessionTime: HTMLInputElement) {
    this.sessionTime = parseInt(sessionTime.value, 10);
    this.changedSessionTime.emit({
      value: this.sessionTime
    });
  }
}
