"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
//our root app component
var core_1 = require('@angular/core');
var Break = (function () {
    function Break() {
        this.breakTime = 0;
        this.breakDisabled = 0;
        this.changedBreakTime = new core_1.EventEmitter();
    }
    Break.prototype.onValueChanged = function (breakTime) {
        this.breakTime = parseInt(breakTime.value, 10);
        this.changedBreakTime.emit({
            value: this.breakTime
        });
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], Break.prototype, "breakTime", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], Break.prototype, "breakDisabled", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], Break.prototype, "changedBreakTime", void 0);
    Break = __decorate([
        core_1.Component({
            selector: 'break-length',
            providers: [],
            template: "\n    <div class=\"ui circular segment center\">\n      <h2>Break Time:</h2>\n      <h3><input name=\"breakTime\" #breakTimeValue type=\"number\" min=\"1\" max=\"120\" [disabled]=\"breakDisabled\" (change)=\"onValueChanged(breakTimeValue)\" value={{breakTime}}/> minutes</h3>\n    </div>\n  ",
            directives: []
        }), 
        __metadata('design:paramtypes', [])
    ], Break);
    return Break;
}());
exports.Break = Break;
//# sourceMappingURL=break.js.map