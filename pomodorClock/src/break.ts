//our root app component
import {Component, Input, Output, EventEmitter} from '@angular/core'

@Component({
  selector: 'break-length',
  providers: [],
  template: `
    <div class="ui circular segment center">
      <h2>Break Time:</h2>
      <h3><input name="breakTime" #breakTimeValue type="number" min="1" max="120" [disabled]="breakDisabled" (change)="onValueChanged(breakTimeValue)" value={{breakTime}}/> minutes</h3>
    </div>
  `,
  directives: []
})

export class Break {
  @Input() breakTime = 0;
  @Input() breakDisabled = 0;
  @Output() changedBreakTime = new EventEmitter();
  constructor() {
  }
  onValueChanged(breakTime: HTMLInputElement) {
    this.breakTime = parseInt(breakTime.value, 10);
    this.changedBreakTime.emit({
      value: this.breakTime
    });
  }
}
