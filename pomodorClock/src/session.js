"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
//our root app component
var core_1 = require('@angular/core');
var Session = (function () {
    function Session() {
        this.sessionTime = 0;
        this.sessionDisabled = false;
        this.changedSessionTime = new core_1.EventEmitter();
    }
    Session.prototype.onValueChanged = function (sessionTime) {
        this.sessionTime = parseInt(sessionTime.value, 10);
        this.changedSessionTime.emit({
            value: this.sessionTime
        });
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], Session.prototype, "sessionTime", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], Session.prototype, "sessionDisabled", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], Session.prototype, "changedSessionTime", void 0);
    Session = __decorate([
        core_1.Component({
            selector: 'session-length',
            providers: [],
            template: "\n    <div class=\"ui circular segment center\">\n      <h2>Session Time:</h2>\n      <h3><input name=\"sessionTime\" #sessionTimeValue type=\"number\" min=\"1\" max=\"60\" [disabled]=\"sessionDisabled\" (change)=\"onValueChanged(sessionTimeValue)\" value={{sessionTime}}> minutes</h3>\n    </div>\n  ",
            directives: []
        }), 
        __metadata('design:paramtypes', [])
    ], Session);
    return Session;
}());
exports.Session = Session;
//# sourceMappingURL=session.js.map