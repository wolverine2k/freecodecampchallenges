//our root app component
import {Component} from '@angular/core'
import {Break} from './break'
import {Session} from './session'
import {Observable} from 'rxjs/Rx';

@Component({
  selector: 'app',
  providers: [],
  template: `
    <div>
      <div class="ui section divider">
      </div>
      <div class="label">Click on the progress bar to start/stop timer!</div>
      <div class="ui indicating progress" (click)="startStopProgress()" data-value="0"  data-total="100" id="myProgressBar">
        <div class="bar">
          <div class="progress"></div>
        </div>
      </div>
      <div class="label">{{pgLabel}}</div>

      <div class="ui section divider">
      </div>
      <div class="ui centered grid">
        <session-length [sessionTime]="sessionTime" (changedSessionTime)="sessionTimeChanged($event)" [sessionDisabled]="inputDisabled"></session-length>
        <break-length [breakTime]="breakTime" (changedBreakTime)="breakTimeChanged($event)" [breakDisabled]="inputDisabled"></break-length>
      </div>
    </div>
  `,
  directives: [Session, Break]
})

export class App {
  sessionTime: Integer = 25;
  breakTime: Integer = 5;
  timeLeft: Integer = this.sessionTime * 60;
  pgLabel: String = "";
  /* True if Session runs otherwise false (i.e. if on break)*/
  isSessionRunning: Boolean = true;
  isClockRunning: Boolean = false;
  resetProgressBar: Boolean = false;
  inputDisabled: Boolean = false;
  runTimerID = 0;

  constructor() {
    $("#myProgressBar").progress({total: this.timeLeft});
  }
  resetProgressBarFunction(): void {
    if(this.resetProgressBar) {
      $("#myProgressBar").progress("reset");
      this.resetProgressBar = false;

      if(this.isSessionRunning) {
        this.timeLeft = this.sessionTime * 60;
      } else {
        this.timeLeft = this.breakTime * 60;
      }
      $("#myProgressBar").progress("reset");
      $("#myProgressBar").progress({total: this.timeLeft});
    }
  }
  updateTimer(): void {
    this.timeLeft -= 1;
    this.resetProgressBarFunction();
    if(this.timeLeft < 0) {
      /* Play sound here and swap the sessions... */
      var wav = 'https://notificationsounds.com/wake-up-tones/alarm-frenzy-493/download/mp3';
      var audio = new Audio(wav);
			audio.play();

      this.isSessionRunning = !this.isSessionRunning;
      if(this.isSessionRunning) {
        this.timeLeft = this.sessionTime * 60;
      } else {
        this.timeLeft = this.breakTime * 60;
      }
      $("#myProgressBar").progress("reset");
      $("#myProgressBar").progress({total: this.timeLeft});
    }
    if(this.isSessionRunning) {
      this.pgLabel = "Ongoing Session, Time Left = " + this.timeLeft + " seconds";
    } else {
     this.pgLabel = "Ongoing Break, Time Left = " + this.timeLeft + " seconds";
    }
    $("#myProgressBar").progress("increment");
  }
  startStopProgress(): void {
    console.log("SessionTime: " +  this.sessionTime + " BreakTime: " + this.breakTime);
/*
    if(this.isSessionRunning) {
      this.timeLeft = this.sessionTime * 60;
    } else {
      this.timeLeft = this.breakTime * 60;
    }
*/
    if(!this.isClockRunning) {
      /* Function should run every second */
      this.runTimerID = setInterval(function() {this.updateTimer()}.bind(this), 1000);
      this.inputDisabled = true;
    } else {
      clearInterval(this.runTimerID);
      this.inputDisabled = false;
    }
    this.isClockRunning = !this.isClockRunning;
  }
  sessionTimeChanged($event): void {
    if(!this.isClockRunning) {
      this.sessionTime = $event.value;
      this.resetProgressBar = true;
    }
  }
  breakTimeChanged($event): void {
    if(!this.isClockRunning) {
      this.breakTime = $event.value;
      this.resetProgressBar = true;
    }
  }
}
